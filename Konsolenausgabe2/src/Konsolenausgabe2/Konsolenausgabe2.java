package Konsolenausgabe2;

public class Konsolenausgabe2 {

	public static void main(String[] args) {
		
		//Aufgabe 1
		String s = "*";
		
		System.out.printf("%4s%s\n", s, s);
		System.out.printf("%-4s"+"%4s\n", s, s);
		System.out.printf("%-4s"+"%4s\n", s, s);
		System.out.printf("%4s%s\n", s, s);
		
		System.out.printf("\n");
		
		// Aufgabe 2
		
		System.out.printf("%-5s= %-19s=%4s\n", "0!", " ", "1");
		System.out.printf("%-5s= %-19s=%4s\n", "1!", "1", "1");
		System.out.printf("%-5s= %-19s=%4s\n", "2!", "1 * 2", "2");
		System.out.printf("%-5s= %-19s=%4s\n", "3!", "1 * 2 * 3", "6");
		System.out.printf("%-5s= %-19s=%4s\n", "4!", "1 * 2 * 3 * 4", "24");
		System.out.printf("%-5s= %-19s=%4s\n", "5!", "1 * 2 * 3 * 4 * 5", "120");
		
		System.out.printf("\n");
		
		// Aufgabe 3 
		
		int[] fahrenheit = {-20, -10, 0, 20, 30};
		double[] celsius = {-28.8889, -23.3333, -17.7778, -6.6667, -1.1111};
		
		
		System.out.printf("%-12s|%10s\n", "Fahrenheit", "Celsius");
		for (int i = 0; i < 23; i++) {
			  System.out.print("-");
			}
		
		for (int i = 0; i < 5; i++) {
			System.out.printf("\n%+-12d|%10.2f", fahrenheit[i], celsius[i]);
		}
	}

}
