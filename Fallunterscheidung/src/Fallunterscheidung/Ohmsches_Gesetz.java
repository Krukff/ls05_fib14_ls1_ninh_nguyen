package Fallunterscheidung;

import java.util.Scanner;

public class Ohmsches_Gesetz {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double u = 0;
		double i = 0;
		double r = 0;
		char kon;
		
		Scanner myScanner = new Scanner(System.in);
		
		while(u == 0 || i == 0 || r == 0) {
			System.out.println("Welche Variable soll berechnet werden? (R, U, I)");
			kon = myScanner.next().charAt(0);
			if (kon == 'R') {
				System.out.println("Spannung U: ");
				u = myScanner.nextDouble();
				
				System.out.println("Stromstärke I: ");
				i = myScanner.nextDouble();
				
				r = u / i;
				
				System.out.println("Der Widerstand ist: " + r + " Ω");
			}
			else if(kon == 'U') {
				System.out.println("Widerstand R: ");
				r = myScanner.nextDouble();
				
				System.out.println("Stromstärke I: ");
				i = myScanner.nextDouble();
				
				u = r * i;
				
				System.out.println("Die Spannung ist: " + u + " V");
			}
			else if(kon == 'I') {
				System.out.println("Widerstand R: ");
				r = myScanner.nextDouble();
				
				System.out.println("Spannung U: ");
				u = myScanner.nextDouble();
				
				i = u /r;
				
				System.out.println("Die Stromstärke ist: " + i + " A");
			}
			else {
				System.out.println("Dies ist keine gültige Variable");
			}
		}
	}

}
