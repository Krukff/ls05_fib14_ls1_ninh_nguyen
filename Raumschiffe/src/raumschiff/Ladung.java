package raumschiff;

public class Ladung {
	private String bezeichnung;
	private int menge;
	
	Ladung() {
		this.bezeichnung = "";
		this.menge = 0;
	}
	
	Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	@Override
	public String toString() {
		return "\nLadung: \n\t bezeichnung=" + bezeichnung + "\t menge=" + menge;
	}

	public int getMenge() {
		return menge;
	}
	public void setMenge(int menge) {
		this.menge = menge;
	}
	
}
