package raumschiff;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {
	public static void main(String[] args) 
	 {
		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung("Borg-Schrott", 5);
		Ladung l3 = new Ladung("Rote Materie", 2);
		Ladung l4 = new Ladung("Forschungssonde", 35);
		Ladung l5 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung l6 = new Ladung("Plasma-Waffe", 50);
		Ladung l7 = new Ladung("Photonentorpedo", 3);
		
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, "IKS Hegh'ta", 2, new ArrayList<Ladung>(Arrays.asList(l1, l5)));
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, "IRW Khazara", 2, new ArrayList<Ladung>(Arrays.asList(l2, l3, l6)));
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, "Ni'Var", 5, new ArrayList<Ladung>(Arrays.asList(l4, l7)));
		
		System.out.println(klingonen);
		System.out.println(romulaner.getLadungsverzeichnis());
		System.out.println(vulkanier.getLadungsverzeichnis());
	
	 }
}
