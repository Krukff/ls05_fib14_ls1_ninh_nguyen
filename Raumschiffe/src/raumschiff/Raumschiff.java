package raumschiff;

import java.util.ArrayList;

public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgung;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemInProzent;
	private String schiffsname;
	private int androidenAnzahl;
	private ArrayList<Ladung> ladungsverzeichnis;
	private static ArrayList<Broadcast> broadcast = new ArrayList<>();
	
	Raumschiff() {
		this.photonentorpedoAnzahl = 1;
		this.energieversorgung = 100;
		this.schildeInProzent = 100;
		this.huelleInProzent = 100;
		this.lebenserhaltungssystemInProzent = 100;
		this.schiffsname = "Enterprise";
		this.androidenAnzahl = 1;
	}
	
	Raumschiff(
			int photonentorpedoAnzahl, 
			int energieversorgung, 
			int schildeInProzent,
			int huelleInProzent,
			int lebenserhaltungssystemInProzent,
			String schiffsname,
			int androidenAnzahl,
			ArrayList<Ladung> ladungsverzeichnis
			) 
	{
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgung = energieversorgung;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemInProzent = lebenserhaltungssystemInProzent;
		this.schiffsname = schiffsname;
		this.androidenAnzahl = androidenAnzahl;
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	
	@Override
	public String toString() {
		return "Raumschiff( " + schiffsname + "): "
				+ "\n photonentorpedoAnzahl=" + photonentorpedoAnzahl 
				+ "\n energieversorgung=" + energieversorgung
				+ "\n schildeInProzent=" + schildeInProzent 
				+ "\n huelleInProzent=" + huelleInProzent
				+ "\n lebenserhaltungssystemInProzent=" + lebenserhaltungssystemInProzent 
				+ "\n androidenAnzahl=" + androidenAnzahl 
				+ "\n ladungsverzeichnis:" + ladungsverzeichnis;
	}

	public void addLadung(Ladung ladung) {
		this.ladungsverzeichnis.add(ladung);
	}
	
	public void torschießen() {
		if (this.photonentorpedoAnzahl == 0) {
			Broadcast nachricht = new Broadcast("\"-=*Click*=-\"");
			nachricht.schiffsname = getSchiffsname();
			addNachricht(nachricht);
		}
		else {
			this.photonentorpedoAnzahl -= 1;
			Broadcast nachricht = new Broadcast("Photonentorpedo abgeschossen!");
			addNachricht(nachricht);
		}
	}
	
	public void phasschießen() {
		if (this.energieversorgung < 50) {
			Broadcast nachricht = new Broadcast("\"-=*Click*=-\"");
			addNachricht(nachricht);
		}
		else {
			this.energieversorgung -= 50;
			Broadcast nachricht = new Broadcast("Phaserkanone abgeschossen!");
			addNachricht(nachricht);
		}
	}
	
	public static void nachrichtenVerzeichnis() {
		for(Broadcast B : broadcast) {
			System.out.println(" Nachrichten von: " + B.schiffsname + " \n");
			System.out.println(" Nachrichten: " + B.nachricht + " \n");
		}
	}
	
	public void addNachricht(Broadcast Nachricht) {
		broadcast.add(Nachricht);
	}
	
	public void sendNachricht(String Nachricht) {
		System.out.println(" Raumschiff sendet eine Nachricht: " + Nachricht);
	}
	
	private void treffer(Raumschiff Raumschiff) {
		System.out.println(" Raumschiff " + this.schiffsname + "wurde getroffen.");
	}
	
	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}



	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}



	public int getEnergieversorgung() {
		return energieversorgung;
	}



	public void setEnergieversorgung(int energieversorgung) {
		this.energieversorgung = energieversorgung;
	}



	public int getSchildeInProzent() {
		return schildeInProzent;
	}



	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}



	public int getHuelleInProzent() {
		return huelleInProzent;
	}



	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}



	public int getLebenserhaltungssystemInProzent() {
		return lebenserhaltungssystemInProzent;
	}



	public void setLebenserhaltungssystemInProzent(int lebenserhaltungssystemInProzent) {
		this.lebenserhaltungssystemInProzent = lebenserhaltungssystemInProzent;
	}



	public String getSchiffsname() {
		return schiffsname;
	}



	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}



	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}



	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

}
