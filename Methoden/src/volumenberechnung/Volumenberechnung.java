package volumenberechnung;

import java.util.Scanner;

public class Volumenberechnung {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.print("Welches Volumen soll berechnet werden? ");
		
		String name = tastatur.nextLine();
		
		double ergebnis = volumen(name);
		
		System.out.print("Das Ergebnis ist: " + ergebnis);
		
		
	}
	
	static double volumen(String objekt) {
		
		Scanner tastatur = new Scanner(System.in);
		double ergebnis;
		double a, b, c, h, r;
		boolean wert = true;
		
		while(wert) {
			switch(objekt) {
			
			case "Würfel":
				System.out.print("Seitenlänge a: ");
				a = tastatur.nextDouble();
				ergebnis = a * a * a;
				wert = false;
				return ergebnis;
				
			case "Quader":
				System.out.print("Seitenlänge a: ");
				a = tastatur.nextDouble();
				
				System.out.print("Seitenlänge b: ");
				b = tastatur.nextDouble();
				
				System.out.print("Seitenlänge c: ");
				c = tastatur.nextDouble();
				ergebnis = a * b * c;
				wert = false;
				return ergebnis;
				
			case "Pyramide":
				System.out.print("Seitenlänge a: ");
				a = tastatur.nextDouble();
				
				System.out.print("Länge h: ");
				h = tastatur.nextDouble();
				
				ergebnis = a * a * h/3;
				wert = false;
				return ergebnis;
				
			case "Kugel":
				System.out.print("Länge r: ");
				r = tastatur.nextDouble();
				
				ergebnis =  4/3 * Math.pow(r, 3) * Math.PI; 
				wert = false;
				
				return ergebnis;
				
			default:
				System.out.print("Fehler! Neue Angabe\n");
				objekt = tastatur.nextLine();
				
						
			}
		
		
		}
		return 0;
		
		
	}
}
