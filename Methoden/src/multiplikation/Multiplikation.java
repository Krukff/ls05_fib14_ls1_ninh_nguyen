package multiplikation;

import java.util.Scanner;

public class Multiplikation{

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben Sie den ersten Wert zum multiplizieren ein: ");
		double wert1 = myScanner.nextDouble();
		
		System.out.print("Bitte geben Sie den zweiten Wert zum multiplizieren ein: ");
		double wert2 = myScanner.nextDouble();
		
		double ergebnis = multiplizieren(wert1, wert2);
		
		System.out.printf("Das Ergebnis ist: " + ergebnis);
	}
	
	public static double multiplizieren(double a, double b) {
		
		double c = a * b;
		
		return c;
	}

}