package Auswahlstrukturen;

import java.util.Scanner;

public class Schaltjahr {
	public static void main(String[] args) {
		int jahr;
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Welches Jahr soll untersucht werden? ");
		jahr = myScanner.nextInt();
		
		if (jahr % 4 == 0 && (jahr % 100 != 0 || jahr % 400 == 0)){
			System.out.println("Dieses Jahr ist ein Schaltjahr.");
		}
		else {
			System.out.println("Dieses Jahr ist kein Schaltjahr.");
		}
	}
}
