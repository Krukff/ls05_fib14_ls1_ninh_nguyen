package Auswahlstrukturen;

import java.util.Scanner;

public class BMI {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Aufgabe BMI
		int gew;
		double gr;
		double bmi;
		char ge;
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Wie viel wiegen Sie? ");
		gew = myScanner.nextInt();
		
		System.out.println("Wie groß sind Sie in m? ");
		gr = myScanner.nextDouble();
		
		System.out.println("Welches Geschlecht (m/w)? ");
		ge = myScanner.next().charAt(0);
		
		bmi = gew/Math.pow(gr, 2);
		
		if (ge == 'm') {
			if (bmi < 20) {
				System.out.printf("BMI: %.2f \n", bmi);
				System.out.println("Klassifikation: Untergewicht");
			}
			if (bmi < 25 && 20 < bmi ) {
				System.out.printf("BMI: %.2f \n", bmi);
				System.out.println("Klassifikation: Normalgewicht");
			}
			if (bmi > 25) {
				System.out.printf("BMI: %.2f \n", bmi);
				System.out.println("Klassifikation: Übergewicht");
			}	
		}
		else if (ge == 'w'){
			if (bmi < 19) {
				System.out.printf("BMI: %.2f \n", bmi);
				System.out.println("Klassifikation: Untergewicht");
			}
			if (bmi < 24 && 19 < bmi ) {
				System.out.printf("BMI: %.2f \n", bmi);
				System.out.println("Klassifikation: Normalgewicht");
			}
			if (bmi > 24) {
				System.out.printf("BMI: %.2f \n", bmi);
				System.out.println("Klassifikation: Übergewicht");
			}
		}
	}

}
