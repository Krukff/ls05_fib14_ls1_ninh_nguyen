﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;

       
       while(true) {
    	   
	       zuZahlenderBetrag = fahrkartenbestellungErfassen();
	       
	       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	       
	       fahrkartenAusgeben();
	       
	       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
       }
    }
    
    static double fahrkartenbestellungErfassen() { 
        int anzahl = 0;
        int wahl = 0;
        double zuZahlenderBetrag = 0.0;
        boolean kon = true;
        
        Scanner tastatur = new Scanner(System.in);
        
        System.out.println("\nFahrkartenbestellvorgang:");
        for (int i = 0; i < 25; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(100);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        
        System.out.println("\n\n");
        
        System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n"
                + "\s\sEinzelfahrschein Regeltarif AB [2,90 Eur] (1)\n"
                + "\s\sTageskarte Regeltarif AB [8,60 Eur] (2)\n"
                + "\s\sKleingruppen-Tageskarte Regeltarif AB [23,50] (3)");
        
        while(wahl > 3 || wahl < 1) {
        	System.out.println("\nIhre Wahl: ");
        	wahl = tastatur.nextInt();
        	
        	if(wahl == 1) {
        		zuZahlenderBetrag = 2.9;
        	}
        	else if(wahl == 2) {
        		zuZahlenderBetrag = 8.6;
        	}
        	else if(wahl == 3) {
        		zuZahlenderBetrag = 23.5;
        	}
        	else {
        		System.out.print(">>falsche Eingabe<<");
        	}
        }
        while(kon == true) {
	        System.out.print("Anzahl der Tickets: ");
	        anzahl = tastatur.nextInt();
	        if (anzahl <= 10 && anzahl > 0) { 
	        	kon = false;
	        }
	        else {
	        	System.out.println(">> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
	        }
        }

    	return anzahl * zuZahlenderBetrag;
    }
    
    static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        // Geldeinwurf
        // -----------
    	double eingezahlterGesamtbetrag;
        double eingeworfeneMünze;
        
        Scanner tastatur = new Scanner(System.in);
        
    	eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n",(zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;

        }
        return eingezahlterGesamtbetrag;
    }
    
    static void fahrkartenAusgeben() {
        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    	
    }
    
    static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
    	double rückgabebetrag;
    	Scanner tastatur = new Scanner(System.in);
        rückgabebetrag = eingezahlterGesamtbetrag - (zuZahlenderBetrag);
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("\nDer Rückgabebetrag in Höhe von %.2f Euro "
     	   		+ "wird in folgenden Münzen ausgezahlt: \n", rückgabebetrag);

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    }
}



/* 5.
 * Ich habe als Datentyp Integer gewählt zum einen, weil die Ticketanzahl nur ganze Zahlen verwendet werden sollen
 * und dadurch auch weniger Speicher verwendet wird
 * 
 * 6.
 * Bei der Berechnung wird ein double mit einem Integer multipliziert. Dadurch wird ein Double als Ergebnis wieder ausgegeben..
 * 
 */
