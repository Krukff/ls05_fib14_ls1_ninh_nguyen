
public class Konsolenausgabe {

	public static void main(String[] args) {
		
		// Aufgabe 1

		System.out.println("Das ist ein \"Beispielsatz\". \nEin Beispielsatz ist das.");
		
		// Der Unterschied zwischen print und println ist, dass bei print die ausgegebene Zeichenkette normal ausgegeben wird. Bei println wird die Zeichenkette ausgeführt und eine neue Zeile gestartet. Also qasi ein \n wird ausgeführt.
		
		
		// Aufgabe 2
		
		System.out.println("      *");
		System.out.println("     ***");
		System.out.println("    *****");
		System.out.println("   *******");
		System.out.println("  *********");
		System.out.println(" ***********");
		System.out.println("*************");
		System.out.println("     ***");
		System.out.println("     ***");
		
		// Aufgabe 3
		
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;
		
		System.out.printf("%.2f\n", a);
		System.out.printf("%.2f\n", b);
		System.out.printf("%.2f\n", c);
		System.out.printf("%.2f\n", d);
		System.out.printf("%.2f\n", e);
		
	}

}


	