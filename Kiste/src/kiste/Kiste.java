package kiste;

public class Kiste {

	private int hoehe;
	private int breite;
	private int tiefe;
	private String farbe;
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Kiste k1 = new Kiste(50, 50, 50, "blau");
		Kiste k2 = new Kiste(20, 20, 20, "grün");
		Kiste k3 = new Kiste(100, 100, 100, "gelb");
		
		System.out.printf("\nKiste 1: \tVolumen: %d \tFarbe: %s\n", k1.getVolumen(), k1.getFarbe());
		System.out.printf("\nKiste 2: \tVolumen: %d \t\tFarbe: %s\n", k2.getVolumen(), k2.getFarbe());
		System.out.printf("\nKiste 3: \tVolumen: %d \tFarbe: %s\n", k3.getVolumen(), k3.getFarbe());

	}

	
	Kiste(int hoehe, int breite, int tiefe, String farbe) {
		this.hoehe = hoehe;
		this.breite = breite;
		this.tiefe = tiefe;
		this.farbe = farbe;
	}
	
	public int getVolumen() {
		return this.breite * this.hoehe * this.tiefe;
		
	}
	
	public String getFarbe() {
		return this.farbe;
	}

}
