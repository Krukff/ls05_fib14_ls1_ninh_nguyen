package Konfigurationstest;

public class Konfigurationstest{
	public static void main(String[] args){
		// Variablen
		// Aufgabe 1
		
		int cent = 70;
		int maximum = 95;
		
		cent = 80;
		maximum = 50;
		
		// Aufgabe 2
		
		boolean status = true;
		short zahl = -1000;
		float zahl2 = 1.225f;
		char zeichen = '#';
		
		// Aufgabe 3
		
		String satz = "Es ist warm.";
		final float CHECK_NR = 8765;
		
		// Aufgabe 4
		
		/* Optimierung der Datenspeicherung, bessere Datentrennung
		 * 
		 */
		
		// Operatoren
		// Aufgabe 1
		
		int ergebnis;
		int zaehler = 1;
		
		int division = 22/6;
		
		ergebnis = 4 + 8 * 9 - 1;
		zaehler++;
		System.out.println(ergebnis);
		System.out.println(zaehler);
		System.out.println(division);
		
		// Aufgabe 2
		
		int schalter = 10;
		
		boolean logik = schalter > 7 && schalter < 12;
		
		System.out.println(logik);
		
		boolean logik2 = schalter!= 10 ||schalter == 12;
		
		System.out.println(logik2);
		
		// Aufgabe 3
		
		String satzteil1 = "Meine Oma ";
		String satzteil2 = "fährt im ";
		String satzteil3 = "Hühnerstall Motorrad";
		
		System.out.println(satzteil1+satzteil2+satzteil3);
		
	}
}

